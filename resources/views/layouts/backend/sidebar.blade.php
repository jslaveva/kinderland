<!-- START SIDEBAR -->
<div class="sidebar clearfix">

    <ul class="sidebar-panel nav">
        <li><a href="index.html"><span class="icon color5"><i class="fa fa-home"></i></span>Начало</a></li>
        <li><a href="kinder_news.html"><span class="icon color6"><i class="fa fa-newspaper-o"></i></span>Новини</a></li>
        <li><a href="{{route('register-child.create')}}" class="{{menuActiveRoute('register-child.create')}}"><span class="icon color6"><i class="fa fa-plus"></i></span>Регистрация</a></li>
        <li><a href="kinder_menu.html"><span class="icon color6"><i class="fa fa-cutlery"></i></span>Меню</a></li>
        <li><a href="kinder_events.html"><span class="icon color6"><i class="fa fa-calendar-o"></i></span>Събития</a></li>
        <li><a href="#" class="{{menuActiveRoute('group.schedule.create')}} {{menuActiveRoute('group-age')}} {{menuActiveRoute('groups')}}" ><span class="icon color7 "><i class="fa fa-users"></i></span>Групи<span class="caret"></span></a>
            <ul>
                <li><a href="{{route('group-age.index')}}">Възрастови групи</a></li>
                <li><a href="{{route('groups.index')}}">Детски групи</a></li>
            </ul>
        </li>
        <li><a class=" {{menuActiveRoute('parents')}}" href="{{route('parents.index')}}"><span class="icon color8"><i class="fa fa-female"></i></span>Родители</a></li>
        <li><a class="{{menuActiveRoute('teachers')}}" href="{{route('teachers.index')}}"><span class="icon color8"><i class="fa fa-graduation-cap"></i></span>Учители</a></li>
        <li><a class="{{menuActiveRoute('subjects')}}" href="{{route('subjects.index')}}"><span class="icon color8"><i class="fa fa-book"></i></span>Учебен план</a></li>
    </ul>

    <ul class="sidebar-panel nav">
        <li><a href="#"><span class="icon color8"><i class="fa fa-user"></i></span>Профил</a></li>
        <li><a href="#"><span class="icon color8"><i class="fa fa-sign-out"></i></span>Изход</a></li>
    </ul>
</div>