<div class="content">

    <!-- Start Presentation -->
    <div class="presentation">
        <div class="row titles">
            <div class="col-lg-1 col-md-2">
                <span class="icon title-icon color8-bg"><i class="fa fa-plus"></i></span>
            </div>
            <div class="col-lg-10">
                <h1>Добавяне на потребители</h1>
                <h4>попълнете формата за да добавите ново дете и родител към вашето учебно заведение</h4>
            </div>
        </div>

    </div>
    <!-- End Presentation -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-default">
        <div class="row">
            <div class="col-md-12 padding-0">
                <div class="panel panel-transparent">

                    <div class="panel-body">
                        <div role="tabpanel">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-justified" role="tablist">
                                <li role="presentation" class="active"><a href="#child" aria-controls="child" role="tab" data-toggle="tab">Регистрация на дете</a></li>
                                <li role="presentation"><a href="#parent" aria-controls="parent" role="tab" data-toggle="tab">Регистрация на родител</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active padding-b-50" id="child">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-push-2 margin-b-35">
                                                <h2 class="margin-b-0">Регистрация на дете</h2>
                                                <h5 class="margin-t-0">попълнете необходимата информация за да добавите ново дете към системата на учебното заведение</h5>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-10 profile-center">
                                                <img src="/img/image_preview.png" alt="img" class="profile-img">
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-10 profile-center">
                                                <a href="#" class="btn btn-default"><i class="fa fa-picture-o"></i>Качи снимка</a>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label for="input002" class="col-sm-2 control-label form-label">Имена</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="input002">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label for="input002" class="col-sm-2 control-label form-label">ЕГН</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="input002">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label form-label">Група</label>
                                            <div class="col-sm-10">
                                                <select class="col-md-12 selectpicker selectpicker required" required>
                                                    <option>1-а група "Калинка"</option>
                                                    <option>1-б група "Калинка"</option>
                                                    <option>2-а група "Калинка"</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <div role="tabpanel" class="tab-pane" id="parent">
                                    <form class="form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-push-2">
                                                <h2 class="margin-b-0">Родителят има регистрация?</h2>
                                                <h5 class="margin-t-0">ако родителят вече има регистрация в системата, моля изберете името му от списъка</h5>
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <label class="col-sm-2 control-label form-label"></label>
                                                <div class="col-sm-10">
                                                    <select class="col-md-12 selectpicker selectpicker required" required>
                                                        <option>Иван Иванов</option>
                                                        <option>Иван Иванов</option>
                                                        <option>Иван Иванов</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-push-2 margin-b-35">
                                                <h2 class="margin-b-0">Регистрация на родител</h2>
                                                <h5 class="margin-t-0">попълнете необходимата информация за да добавите нов родител към системата на учебното заведение</h5>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label for="input002" class="col-sm-2 control-label form-label">Имена</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="input002">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label for="input002" class="col-sm-2 control-label form-label">Телефон</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="input002">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label for="input002" class="col-sm-2 control-label form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" id="input002">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label for="input002" class="col-sm-2 control-label form-label">Парола</label>
                                            <div class="col-sm-10">
                                                <input type="password" value="231231231" readonly class="form-control" id="input002">
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label form-label">Адрес</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" rows="3" id="textarea1" ></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label form-label"></label>
                                            <div class="col-sm-10">
                                                <button  type="submit" class="btn btn-default pull-right">Запази</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- END CONTAINER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->


    <!-- Start Footer -->
    <div class="row footer">

    </div>
    <!-- End Footer -->


</div>
<!-- End Content -->