<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <title>@yield('title') | Web Kinderland </title>

    <!-- ========== Css Files ========== -->
    <link href="{{ asset('css/backend/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/backend/plugin/simplemde.css') }}" rel="stylesheet">

    <style>

    </style>

</head>
<body>
<!-- Start Page Loading -->
<div class="loading"><img src="/img/loading.gif" alt="loading-img"></div>

<div id="top" class="clearfix">

    <!-- Start App Logo -->
    <div class="applogo">
        <a href="index.html" class="logo"><img src="/img/logo.jpg" alt=""></a>
    </div>
    <!-- End App Logo -->

    <!-- Start Sidebar Show Hide Button -->
    <a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a>
    <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a>
    <!-- End Sidebar Show Hide Button -->


    <!-- Start Top Right -->
    <ul class="top-right">
        <li class="dropdown link">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox"><b>{{ Auth::user()->name}}</b><span class="caret"></span></a>
        </li>
    </ul>
    <!-- End Top Right -->
</div>
<!-- END TOP -->

@include('layouts.backend.sidebar')

<div class="content">
    @yield('content')



</div>




<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>

<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>

<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->

<script type="text/javascript" src="{{ asset('js/sweet-alert/sweet-alert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-select/bootstrap-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/simplemde.js') }}"></script>

@yield('scripts')


@if(session()->has('flash_message'))
    <script>
        swal({
            title: "{!!session('flash_message.title')!!}",
            text: '{!! session('flash_message.message') !!}',
            type: "{{session('flash_message.notice')}}",
            showConfirmButton: "{{session('flash_message.overlay')}}",
            timer: "{{session('flash_message.timer')}}"
        });
    </script>
@endif

</body>