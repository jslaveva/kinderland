<nav class="navbar main-navigaion">
    <button class="navbar-toggler hidden-md-up" type="button" data-toggle="collapse" data-target="#navbar-header" aria-controls="navbar-header">
        &#9776;
    </button>
    <div class="container">
        <div class="col-lg-4">
            <a class="navbar-brand" href="{{route('home')}}"><img src="/img/layout/logo.png" alt=""></a>
        </div>
        <div class="collapse navbar-toggleable-sm" id="navbar-header">
            <div class="col-lg-8">
                <ul class="nav navbar-nav ">
                    <li class="nav-item {{menuActiveRoute('home')}} ">
                        <a class="nav-link" href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i> Начало <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown {{menuActiveRoute('schedule')}} {{menuActiveRoute('news')}} {{menuActiveRoute('team')}} ">
                        <a class="nav-link" href="#" id="dropdownMenuAbout" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-users" aria-hidden="true"></i>За нас</a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuAbout">
                            <a class="dropdown-item {{menuActiveRoute('news')}}"   href="{{route('news')}}">Новини</a>
                            <a class="dropdown-item {{menuActiveRoute('team')}}" href="{{route('team')}}">Екипи</a>
                            <a class="dropdown-item" href="menu.html">Меню</a>
                            <a class="dropdown-item {{menuActiveRoute('schedule')}}" href="{{route('schedule')}}">Учебен план</a>
                        </div>
                    </li>
                    <li class="nav-item {{menuActiveRoute('groups')}}">
                        <a class="nav-link" href="{{route('groups')}}"><i class="fa fa-child" aria-hidden="true"></i>Групи</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="portfolio.html"><i class="fa fa-info-circle" aria-hidden="true"></i>Портфолио</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contacts.html"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Контакти</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" id="dropdownMenuProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>Профил</a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuProfile">
                            <a class="dropdown-item" href="#">Лични данни</a>
                            <a class="dropdown-item" href="#">Съобщения</a>
                            <a class="dropdown-item" href="#">Събития</a>
                            <a class="dropdown-item" href="#">Изход</a>
                        </div>
                    </li>
                    <!--<li class="nav-item">-->
                    <!--<a class="nav-link" href="#"><i class="fa fa-external-link" aria-hidden="true"></i>Вход</a>-->
                    <!--</li>-->
                </ul>
            </div>
        </div>
    </div>
</nav> <!-- /navbar -->