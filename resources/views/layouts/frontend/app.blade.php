<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <link rel="shortcut icon" href="images/layout/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/layout/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,400i,700&subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rubik+One&subset=cyrillic" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('css/lightbox.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <title>@yield('title')</title>
</head>


<body class="@yield('page-name')">
@include('layouts.frontend.navigation')
<div class="content-wrapper clearfix">
    @yield('header')
    @yield('content')
</div>

@include('layouts.frontend.footer')

<!-- Scripts -->

<script src="{{ asset('js/frontend/jquery.min.js') }}"></script>
<script src="{{ asset('js/frontend/jquery.parallax.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
<script src="{{ asset('js/frontend/jquery-ui-1.10.4.min.js') }}"></script>
<script src="{{ asset('js/frontend/jquery.isotope.min.js') }}"></script>
<script src="{{ asset('js/frontend/animated-masonry-gallery.js') }}"></script>
<script src="{{ asset('js/frontend/slick.js') }}"></script>
<script src="{{ asset('js/frontend/bootstrap.min.js') }}"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('js/frontend/ie10-viewport-bug-workaround.js') }}"></script>
<script src="{{ asset('js/frontend/lightbox-plus-jquery.min.js') }}"></script>
<script src="{{ asset('js/frontend/main.js') }}"></script>


</body>
</html>
