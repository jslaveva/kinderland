<header class="header">
    <div class="header-image-box">
        <h1 class="text-xs-center decorative">{{$title}}</h1>
    </div>
    <!--/.header-image-box-->
</header>