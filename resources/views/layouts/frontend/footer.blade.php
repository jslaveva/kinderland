<footer class="footer">
    <div class="footer-container">
        <div class="container">
            <div class="col-lg-6 col-xs-12">
                <h3>За нас</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consectetur cupiditate error facere, fuga iure iusto, magni quam, quidem sapiente sint vitae voluptas! Accusantium animi, deserunt esse nam non vitae.</p>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12">
                <h3>Дни за консултация</h3>
                <ul class="list-unstyled">
                    <li><span>Вторник:</span>14:30 - 16:00</li>
                    <li><span>Сряда:</span>17:30 - 18:00</li>
                    <li><span>Петък:</span>16:30 - 18:00</li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6  col-xs-12">
                <h3>Контакти</h3>
                <small>за контакт с г-ца Иванова</small>
                <ul class="list-unstyled">
                    <li><span>Телефон:</span>088556468</li>
                    <li><span>Email:</span> <a href="#">ivanova@mail.com</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--/.footer-container-->
    <div class="footer-wrapper">
        <div class="container">
            <div class="col-xs-12">
                <div class="text-xs-center">
                    <ul class="list-inline socials-list">
                        <li class="list-inline-item"><a href="#" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="gplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a href="#" class="tweeter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    </ul>
                    <!--/.socials-list-->
                </div>
                <div class="text-xs-center"><small>©&nbsp;Всички права запазени, дизайн Йоана Славева</small></div>
            </div>
        </div>
    </div>
    <!--/.footer-wrapper-->
</footer>


<!--/.footer-->
<span id="to-top-link-block" class="hidden">
    <a href="#top" class="well well-sm" onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
        <i class="fa fa-chevron-up" aria-hidden="true"></i>
    </a>
</span>