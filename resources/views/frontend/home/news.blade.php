<div class="slider-wrapper clearfix">
    <div class="container">
        <div class="clearfix">
            <div class="col-xs-12">
                <h2>Новини</h2>
            </div>
        </div>
        <div class="news-slider">
            <div class="col-sm-4"><a href="#"><figure class="thumb"><img src="/img/layout/thumb.jpg" alt=""></figure><div class="slide-text"><strong>Курс по рисуване</strong><span><i class="fa fa-clock-o" aria-hidden="true"></i>28.09.2016</span><span  class="rating"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span></div></a></div>
            <div class="col-sm-4"><a href="#"><figure class="thumb"><img src="/img/layout/thumb.jpg" alt=""></figure><div class="slide-text"><strong>Курс по рисуване</strong><span><i class="fa fa-clock-o" aria-hidden="true"></i>28.09.2016</span><span  class="rating"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span></div></a></div>
            <div class="col-sm-4"><a href="#"><figure class="thumb"><img src="/img/layout/thumb.jpg" alt=""></figure><div class="slide-text"><strong>Курс по рисуване</strong><span><i class="fa fa-clock-o" aria-hidden="true"></i>28.09.2016</span><span  class="rating"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span></div></a></div>
            <div class="col-sm-4"><a href="#"><figure class="thumb"><img src="/img/layout/thumb.jpg" alt=""></figure><div class="slide-text"><strong>Курс по рисуване</strong><span><i class="fa fa-clock-o" aria-hidden="true"></i>28.09.2016</span><span  class="rating"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span></div></a></div>
            <div class="col-sm-4"><a href="#"><figure class="thumb"><img src="/img/layout/thumb.jpg" alt=""></figure><div class="slide-text"><strong>Курс по рисуване</strong><span><i class="fa fa-clock-o" aria-hidden="true"></i>28.09.2016</span><span  class="rating"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span></div></a></div>
            <div class="col-sm-4"><a href="#"><figure class="thumb"><img src="/img/layout/thumb.jpg" alt=""></figure><div class="slide-text"><strong>Курс по рисуване</strong><span><i class="fa fa-clock-o" aria-hidden="true"></i>28.09.2016</span><span  class="rating"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span></div></a></div>
            <div class="col-sm-4"><a href="#"><figure class="thumb"><img src="/img/layout/thumb.jpg" alt=""></figure><div class="slide-text"><strong>Курс по рисуване</strong><span><i class="fa fa-clock-o" aria-hidden="true"></i>28.09.2016</span><span  class="rating"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span></div></a></div>
            <div class="col-sm-4"><a href="#"><figure class="thumb"><img src="/img/layout/thumb.jpg" alt=""></figure><div class="slide-text"><strong>Курс по рисуване</strong><span><i class="fa fa-clock-o" aria-hidden="true"></i>28.09.2016</span><span  class="rating"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span></div></a></div>
        </div>
        <!--/.news-slider-->
    </div>
</div>
<!--/.slider-wrapper-->