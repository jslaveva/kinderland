<div class="masonry-gallery clearfix">
    <div class="container">
        <div id="gallery">
            <div id="gallery-header">
                <div id="gallery-header-center">
                    <div id="gallery-header-center-left " class="col-xl-5">
                        <div id="gallery-header-center-left-title">Упражнения</div>
                    </div>
                    <div id="gallery-header-center-right" class="col-xl-7">
                        <div class="gallery-header-center-right-links" id="filter-all">всички</div>
                        <div class="gallery-header-center-right-links" id="filter-party">празненства</div>
                        <div class="gallery-header-center-right-links" id="filter-sport">спорт</div>
                        <div class="gallery-header-center-right-links" id="filter-work">изкуство</div>
                        <div class="gallery-header-center-right-links" id="filter-study">обучение</div>
                    </div>
                </div>
            </div>
            <div id="gallery-content">
                <div id="gallery-content-center">
                    <a href="/img/layout/gallery/party1.jpg" data-lightbox="gallery-thumb" class="thumb all party gallery-thumb "> <img src="/img/layout/gallery/party1.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/art1.jpg" data-lightbox="gallery-thumb" class="thumb all work gallery-thumb">  <img src="/img/layout/gallery/art1.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/art2.jpg" data-lightbox="gallery-thumb" class="thumb all work gallery-thumb">  <img src="/img/layout/gallery/art2.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/party2.jpg" data-lightbox="gallery-thumb" class="thumb all party gallery-thumb">  <img src="/img/layout/gallery/party2.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/sport1.jpg" data-lightbox="gallery-thumb" class="thumb all sport gallery-thumb"><img src="/img/layout/gallery/sport1.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/party3.jpg" data-lightbox="gallery-thumb" class="thumb all party gallery-thumb"><img src="/img/layout/gallery/party3.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/sport5.jpg" data-lightbox="gallery-thumb" class="thumb all sport gallery-thumb"><img src="/img/layout/gallery/sport5.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/sport2.jpg" data-lightbox="gallery-thumb" class="thumb all sport gallery-thumb"><img src="/img/layout/gallery/sport2.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/sport3.jpg" data-lightbox="gallery-thumb" class="thumb all sport gallery-thumb"><img src="/img/layout/gallery/sport3.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/study4.jpg"  data-lightbox="gallery-thumb" class="thumb all study gallery-thumb"><img src="/img/layout/gallery/study4.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/party4.jpg" data-lightbox="gallery-thumb" class="thumb all party gallery-thumb"> <img src="/img/layout/gallery/party4.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/art4.jpg" data-lightbox="gallery-thumb" class="thumb all work gallery-thumb"> <img src="/img/layout/gallery/art4.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/study3.jpg" data-lightbox="gallery-thumb" class="thumb all study gallery-thumb"><img src="/img/layout/gallery/study3.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/party5.jpg" data-lightbox="gallery-thumb" class="thumb all party gallery-thumb"><img src="/img/layout/gallery/party5.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/study2.jpg" data-lightbox="gallery-thumb" class="thumb all study gallery-thumb"><img src="/img/layout/gallery/study2.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/sport4.jpg" data-lightbox="gallery-thumb" class="thumb all sport gallery-thumb"> <img src="/img/layout/gallery/sport4.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/art3.jpg" data-lightbox="gallery-thumb" class="thumb all work gallery-thumb"><img src="/img/layout/gallery/art3.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/sport6.jpg" data-lightbox="gallery-thumb" class="thumb all sport gallery-thumb"><img src="/img/layout/gallery/sport6.jpg" alt=""  class=""></a>
                    <a href="/img/layout/gallery/study1.jpg" data-lightbox="gallery-thumb" class="thumb all study gallery-thumb"> <img src="/img/layout/gallery/study1.jpg" alt=""  class=""></a> </div>
            </div>
        </div>
    </div>
</div>
<!--/.masonry-gallery-->