<div class="qoutes-carousel-container clearfix">
    <div class="container">
        <div class="qoutes-slider">
            <blockquote class="slick-slide qoute-block">
                <div class="col-sm-8 offset-sm-2">
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda aut dolor exercitationem harum minima nihil numquam, optio placeat quaerat voluptatum. Adipisci molestiae, quia! Earum enim impedit mollitia odio repudiandae similique?"
                </div>
            </blockquote>
            <blockquote class="slick-slide qoute-block">
                <div class="col-sm-8 offset-sm-2">
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda aut dolor exercitationem harum minima nihil numquam, optio placeat quaerat voluptatum. Adipisci molestiae, quia! Earum enim impedit mollitia odio repudiandae similique?"
                </div>
            </blockquote>
            <blockquote class="slick-slide qoute-block">
                <div class="col-sm-8 offset-sm-2">
                    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda aut dolor exercitationem harum minima nihil numquam, optio placeat quaerat voluptatum. Adipisci molestiae, quia! Earum enim impedit mollitia odio repudiandae similique?"
                </div>
            </blockquote>
        </div>
    </div>
</div>
<!--/.qoutes-carousel-container-->