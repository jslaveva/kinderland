@extends('layouts.frontend.app')
@section('title', 'Учебен план')
@section('header')
    @include('layouts.frontend.header-inner-page', ['title' => 'Учебен план'])
@endsection


@section('content')
    <div class="clearfix content">

        <div class="container article">
            <div class="col-xs-12">
                <div id="accordion" role="tablist" aria-multiselectable="true">
                    @foreach($subjects as $k => $subject)
                        <div class="card">

                            <div class="card-header" role="tab" id="headingOne-{{$subject->id}}" style="background: #56509F">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" style="color: #fff;" data-parent="#accordion" href="#collapseOne-{{$subject->id}}" aria-expanded="true" aria-controls="collapseOne">
                                        {{$subject->name}}
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne-{{$subject->id}}" class="collapse show {{$k ==0 ? 'in' : ''}} " role="tabpanel" aria-labelledby="headingOne-{{$subject->id}}">
                                <div class="card-block">

                                    @foreach($groupAges as $groupAge)

                                        <table class=" table table-bordered">
                                            <thead class="thead-default">
                                            <tr>
                                                <th>Модул</th>
                                                <th>{{$groupAge->title}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($subject->modules as $module)

                                                <?php $assignments = $module->assignments()->where('group_age_id', $groupAge->id)->get() ?>
                                                @if($assignments->count() == 0)
                                                    @continue
                                                    @endif

                                                <tr>
                                                    <td><strong>{{$module->title}}</strong></td>
                                                    <td>
                                                        @foreach($module->assignments()->where('group_age_id', $groupAge->id)->get() as $assignment)
                                                            {{$assignment->assignment}}
                                                            <br>
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>

                                    @endforeach


                                </div>
                            </div>
                        </div>
                    @endforeach



                </div>
            </div>
        </div>
    </div>
@endsection