<div class="container article">
    <div class="col-lg-6">
        <h2 class="m-b-0">{{$group->title}}</h2>
        <span class="d-block m-b-2">{{$group->groupAge->title}}</span>
        {!! Markdown::convertToHtml($group->description); !!}
    </div>



    @foreach($group->teachers as $teacher)
        <div class="col-sm-6 col-lg-3 text-xs-center">
            <img src="{{ Storage::disk('local')->url($teacher->picture) }}" class="img-circle m-b-1" alt="{{$teacher->name}}">
            <h5>{{$teacher->name}}</h5>
            <span>{{$teacher->type->title}}</span>
        </div>
    @endforeach
</div>


<?php
$days = [ 'Понеделник', 'Вторник', 'Сряда', 'Четвъртък', 'Петък'];

?>

<div class="container article">
    <div class="col-xs-12">
        <h2>Седмично разписание на групата</h2>
        <table class="table text-xs-center table-bordered">
            <thead class="thead-default">
            <tr>
                <th class="text-xs-center">Понеделник</th>
                <th class="text-xs-center">Вторник</th>
                <th class="text-xs-center">Сряда</th>
                <th class="text-xs-center">Четвъртък</th>
                <th class="text-xs-center">Петък</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @foreach($days as $day)
                    <td>
                        @if(in_array($day,  array_keys($group->orderSchedule())))
                            @foreach($group->orderSchedule()[$day] as $hours)
                                {{$hours->subject->name}}<hr />
                            @endforeach
                        @endif
                    </td>
                @endforeach
            </tr>



            </tbody>
        </table>
    </div>
    <div class="col-xs-12">
        <hr>
    </div>
</div>
