@extends('layouts.frontend.app')
@section('title', 'Групи')
@section('header')
    @include('layouts.frontend.header-inner-page', ['title' => 'Групи'])
@endsection


@section('content')
    <div class="clearfix content">
        @foreach($groups as $group)
            @include('frontend.groups.group')
        @endforeach
    </div>
@endsection