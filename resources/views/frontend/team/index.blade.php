@extends('layouts.frontend.app')
@section('title', 'Екип')
@section('header')
    @include('layouts.frontend.header-inner-page', ['title' => 'Екип'])
@endsection


@section('content')
    <div class="clearfix content">

        @foreach($teachers->chunk(3) as $chunk)
            <div class="container article">
                @foreach($chunk as $teacher)
                    <div class="col-lg-4">
                        <img src="{{ Storage::disk('local')->url($teacher->picture) }}" alt="{{$teacher->name}}" class="img-circle m-b-2" >
                        <h2 class="m-b-0">{{$teacher->name}}</h2>
                        <span class="d-block m-b-2">{{$teacher->type->title}}</span>
                        <span class="d-block "><i class="primary-color fa fa-envelope"></i> {{$teacher->email}}</span>
                        <span class="d-block m-b-1"><i class="primary-color fa fa-phone"></i> {{$teacher->phone}}</span>

                        {!! Markdown::convertToHtml($teacher->bio); !!}
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
@endsection