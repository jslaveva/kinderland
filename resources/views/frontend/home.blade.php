@extends('layouts.frontend.app')
@section('title', 'Web kinderland')
@section('page-name', 'index')
@section('header')
    <header class="header">

        <div class="header-slider">
            <div class="slick-slide" style="background-image: url('/img/layout/slider/slide-bg-1.jpg');">
                <div class="container slide-text animated fadeInLeft">
                    <div class="textbox col-md-6">
                        <strong>Здравейте <span>приятели</span></strong>
                        <p>това е Web kinderland</p>
                    </div>

                </div>
            </div>
            <div class="slick-slide" style="background-image: url('/img/layout/slider/slide-bg-2.jpg');">
                <div class="container slide-text animated fadeInLeft">
                    <div class="textbox col-md-6">
                        <strong>Място за <span>информация</span></strong>
                        <p>всичко за игрите и ученето в детската градина</p>
                    </div>
                </div>
            </div>
            <div class="slick-slide" style="background-image: url('/img/layout/slider/slide-bg-3.jpg');">
                <div class="container slide-text animated fadeInLeft">
                    <div class="textbox col-md-6">
                        <strong>Информация за <span>събития</span></strong>
                        <p>предстоящи и отминали събития</p>
                    </div>
                </div>
            </div>
        </div>
    </header>
@endsection

@section('content')

    <div class="intro-content">
        <div class="container animated fadeInLeft">
            <h1 class="text-xs-center decorative">Какво е Web kinderland ? <span>уеб-портал за споделяне на информация между учители и родители</span></h1>
            <div class="col-md-6  animated"  data-animated="fadeInLeft">
                <p>Това е съвременен уеб-портал за споделяне на информация между учители и родители в предучилищния етап на образование. Той цели да подобри комуникацията между учители и родители на деца от предучилищният етап на обучение. Уебсайта има за цел да дава възможност за бързо и удобно следене на учебния и възпитателния процес и съхранение на подробна информация за всяко дете, както и  всичко най-важно свързано с учебния план и ежедневните занимания. </p>
            </div>
            <div class="col-md-6">
                <p>Спестява консумативи, време, респективно финансов ресурс, както за родителите, така и за училището. Предоставя възможност за контрол от директора и неговите помощници на степента на ангажираност на учителите и качестовото на тяхната дейност. Осигурява допълнителна сигурност на информацията при загуба на хартиен носител с информация.Снимки, важна информация и цялата нужна информация на един съвременен родител.Приятни моменти с Web kinderland.</p>
            </div>
        </div>
    </div>
    <!--/.intro-content-->

    <div class="homepage-parallax-container" id="parallax">
        <div class="parallax-textbox container" id="timer-container">
            <div class="col-sm-3 animated  fadeInUp" style="animation-delay: 0.1s">
                <span class="circle">
                     <strong class="timer count-number" data-to="26" data-speed="900">0</strong>
                    <span>ученика</span>
                </span>
            </div>
            <div class="col-sm-3 animated  fadeInUp" style="animation-delay: 0.5s">
                <span class="circle secondary">
                    <strong class="timer count-number" data-to="52" data-speed="1200">0</strong>
                    <span>родители</span>
                </span>
            </div>
            <div class="col-sm-3 animated  fadeInUp" style="animation-delay: 0.7s">
                <span class="circle">
                   <strong class="timer count-number" data-to="86" data-speed="1400">0</strong>
                    <span>статии</span>
                </span>
            </div>
            <div class="col-sm-3 animated  fadeInUp" style="animation-delay: 0.9s">
                <span class="circle secondary">
                    <strong class="timer count-number" data-to="29" data-speed="1600">0</strong>
                    <span>новини</span>
                </span>
            </div>
        </div>
    </div>
    <!--/.homepage-parallax-container-->

    @include('frontend.home.news')

    <div class="extra-container">
        <div class="container text-xs-center text-lg-left">
            <div class="col-lg-8">
                <strong>Как се справя твоето дете ?</strong>
            </div>
            <div class="col-lg-4">
                <a href="#" class="white-btn pull-lg-right">виж тук <i class="fa fa-angle-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <!--/.extra-container-->

    @include('frontend.home.features')
    @include('frontend.home.reviews')
    @include('frontend.home.gallery')


@endsection
