@extends('layouts.frontend.app')
@section('title', 'Новини')
@section('header')
    @include('layouts.frontend.header-inner-page', ['title' => 'Новини'])
@endsection


@section('content')
    <div class="clearfix content">
        <div class="container article">
            <div class="col-xs-12">
                <h2>Lorem ipsum dolor sit amet, consectetur</h2>
            </div>
            <div class="col-md-4">
                <a href="/img/layout/gallery/study2.jpg" data-lightbox="gallery-thumb" class="thumb "> <img src="/img/layout/gallery/study2.jpg" alt="" class=""></a>
            </div>
            <div class="col-md-8">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, dignissimos dolorem dolores dolorum laboriosam libero minus neque nihil provident quisquam, sunt tempora voluptatibus. Accusamus itaque optio, quibusdam repudiandae sed vel.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.  Cum, dignissimos dolorem dolores dolorum laboriosam libero minus neque nihil provident quisquam, sunt tempora voluptatibus. Accusamus itaque optio Cum, dignissimos dolorem dolores dolorum laboriosam libero minus neque nihil provident quisquam, sunt tempora voluptatibus. Accusamus itaque optio, quibusdam repudiandae sed vel.</p>
            </div>
        </div>
        <div class="container article">
            <div class="col-xs-12">
                <h2>Lorem ipsum dolor sit amet, consectetur</h2>
            </div>
            <div class="col-md-4">
                <a href="/img/layout/gallery/study2.jpg" data-lightbox="gallery-thumb" class="thumb "> <img src="/img/layout/gallery/study2.jpg" alt="" class=""></a>
            </div>
            <div class="col-md-8">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, dignissimos dolorem dolores dolorum laboriosam libero minus neque nihil provident quisquam, sunt tempora voluptatibus. Accusamus itaque optio, quibusdam repudiandae sed vel.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.  Cum, dignissimos dolorem dolores dolorum laboriosam libero minus neque nihil provident quisquam, sunt tempora voluptatibus. Accusamus itaque optio Cum, dignissimos dolorem dolores dolorum laboriosam libero minus neque nihil provident quisquam, sunt tempora voluptatibus. Accusamus itaque optio, quibusdam repudiandae sed vel.</p>
            </div>
        </div>
        <div class="container article">
            <div class="col-xs-12">
                <h2>Lorem ipsum dolor sit amet, consectetur</h2>
            </div>
            <div class="col-md-4">
                <a href="/img/layout/gallery/study2.jpg" data-lightbox="gallery-thumb" class="thumb "> <img src="/img/layout/gallery/study2.jpg" alt="" class=""></a>
            </div>
            <div class="col-md-8">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, dignissimos dolorem dolores dolorum laboriosam libero minus neque nihil provident quisquam, sunt tempora voluptatibus. Accusamus itaque optio, quibusdam repudiandae sed vel.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.  Cum, dignissimos dolorem dolores dolorum laboriosam libero minus neque nihil provident quisquam, sunt tempora voluptatibus. Accusamus itaque optio Cum, dignissimos dolorem dolores dolorum laboriosam libero minus neque nihil provident quisquam, sunt tempora voluptatibus. Accusamus itaque optio, quibusdam repudiandae sed vel.</p>
            </div>
        </div>
    </div>
@endsection