@extends('layouts.backend.app')
@section('title', 'Учители')
@section('content')
    <div class="presentation">
        <div class="row titles">
            <div class="col-lg-1 col-md-2">
                <span class="icon title-icon color8-bg"><i class="fa fa-graduation-cap"></i></span>
            </div>
            <div class="col-lg-10">
                <h1>Учители</h1>
                <h4>регистрирани в системата учители</h4>
            </div>
        </div>
    </div>


    <div class="container-padding">
        <div class="row">
            <div class="col-md-12">

                <div class="pull-right">
                    <a class="btn btn-default" href="{{route('teachers.create')}}">
                        <i class="fa fa-plus"></i> Нов учител
                    </a>
                </div>


                <div class="clearfix"></div>
                <br>

                <div class="panel panel-default">
                    <div class="panel-body table-responsive">

                        <div id="example0_wrapper" class="dataTables_wrapper">
                            <div class="dataTables_length" id="example0_length"></div>

                            <table id="example0" class="table display dataTable" role="grid" aria-describedby="example0_info">

                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 253px;">Име</th>
                                    <th>Длъжност</th>
                                    <th>Телефон</th>
                                    <th>Емайл</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($teachers as $index => $teacher)
                                    <tr role="row" class="{{$index % 2 != 0 ? 'odd' : 'even' }}">
                                        <td>{{$teacher->name}}</td>
                                        <td>{{$teacher->type->title}}</td>
                                        <td>{{$teacher->phone}}</td>
                                        <td>{{$teacher->email}}</td>
                                        <td >
                                            <div class="btn-group" role="group" aria-label="...">
                                                <a  href="{{route('teachers.edit', $teacher->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>

                                                <form action="{{ route('teachers.destroy', $teacher->id) }}" method="POST" style="display: inline-block; ">
                                                    {{ csrf_field() }}
                                                    {{method_field('delete')}}
                                                    <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                                                </form>

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        <div class="pull-right">
                            {{$teachers->links()}}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection