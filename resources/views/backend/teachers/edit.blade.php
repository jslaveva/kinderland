@extends('layouts.backend.app')
@section('title', 'Създаване на учителски профил')
@section('content')
    <div class="row presentation">
        <div class="col-lg-8 col-md-6 titles">
            <h1>Добавяне на учител</h1>
            <h4>попълнете формата за да добавите нов учител към вашето учебно заведение</h4>
        </div>
    </div>
    <div class="container-default">
        <form class="form-horizontal" method="post" action="{{route('teachers.update', $teacher->id)}}" enctype="multipart/form-data" >
            {{csrf_field()}}
            {{method_field('put')}}
            <div class="form-group">
                <label class="col-sm-2"></label>
                <div class="col-sm-10 profile-center">

                    <div class="prd-img-prv">
                        <img src="{{ Storage::disk('local')->url($teacher->picture) }}" alt="img" class="profile-img" width="184" height="227">
                    </div>

                </div>
            </div>


            <div class="form-group {{$errors->has('name') ? 'has-error'  : '' }}">
                <label for="input002" class="col-sm-2 control-label form-label">Имена</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control"  value="{{ old('name', $teacher->name) }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group {{$errors->has('email') ? 'has-error'  : '' }} ">
                <label for="input002" class="col-sm-2 control-label form-label">Емайл</label>
                <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" value="{{ old('email', $teacher->email) }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group {{$errors->has('phone') ? 'has-error'  : '' }} ">
                <label for="input002" class="col-sm-2 control-label form-label">Телефон</label>
                <div class="col-sm-10">
                    <input type="text" name="phone" class="form-control" value="{{ old('phone', $teacher->phone) }}">
                    @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group {{$errors->has('bio') ? 'has-error'  : '' }}">
                <label class="col-sm-2 control-label form-label">Биография</label>
                <div class="col-sm-10">
                    <textarea id="bio" name="bio">{{ old('bio', $teacher->bio) }}</textarea>

                    @if ($errors->has('bio'))
                        <span class="help-block">
                            <strong>{{ $errors->first('bio') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label form-label">Длъжност</label>
                <div class="col-sm-10">
                    <select name="teacher_type_id" class="selectpicker selectpicker" style="width: 100%">
                        @foreach($teacherTypes as $id => $title)
                            <option {{ old('teacher_type_id', $teacher->type->id) == $id ? 'selected="selected"' : '' }} value="{{$id}}">{{$title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label form-label"></label>
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-default pull-right">Запази</button>
                </div>
            </div>


        </form>

    </div>


@endsection

@section('scripts')
    <script>
        var simplemde = new SimpleMDE({ element: document.getElementById("bio") });
        function chooseFile(e) {

            $("#fileInput").click();
        }

        function assignFilePreviews() {
            $( 'input[data-previewable=\"true\"]' ).change(function() {
                var prvCnt = $(this).attr('data-preview-container');
                if(prvCnt) {
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var img = $('<img>', {width: '184px', height:'227px'});
                            img.attr('src', e.target.result);
                            img.error(function() {
                                $(prvCnt).html('');
                            });
                            $(prvCnt).html('');
                            img.appendTo(prvCnt);
                        }
                        reader.readAsDataURL(this.files[0]);
                    }
                }
            });
        }
        $(document).ready(function() {
            assignFilePreviews();


        });

    </script>
@endsection