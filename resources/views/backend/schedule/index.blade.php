@extends('layouts.backend.app')
@section('title', 'Учители')
@section('content')
    <div class="presentation">
        <div class="row titles">
            <div class="col-lg-1 col-md-2">
                <span class="icon title-icon color8-bg"><i class="fa fa-book"></i></span>
            </div>
            <div class="col-lg-10">
                <h1>Предмети</h1>
                <h4></h4>
            </div>
        </div>
    </div>


    <div class="container-padding">
        <div class="row">
            <div class="col-md-12">

                <div class="pull-right">
                    <a class="btn btn-default" href="{{route('subjects.create')}}">
                        <i class="fa fa-plus"></i> Нов предмет
                    </a>
                </div>


                <div class="clearfix"></div>
                <br>

                <div class="panel panel-default">
                    <div class="panel-body table-responsive">

                        <div id="example0_wrapper" class="dataTables_wrapper">
                            <div class="dataTables_length" id="example0_length"></div>

                            <table id="example0" class="table display dataTable" role="grid" aria-describedby="example0_info">

                                <thead>
                                <tr role="row">
                                    <th>Име</th>
                                    <th>Брой модули</th>
                                    <th>Добави нов модул</th>
                                    <th>Преглед</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($subjects as $subject)
                                    <tr>
                                        <td>{{$subject->name}}</td>
                                        <td>{{$subject->modules_count}}</td>
                                        <td>
                                            <a href="{{route('subjects.modules.create', $subject->id)}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                        </td>
                                        <td>
                                            <a href="" class="btn btn-default"><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>

                            <div class="pull-right">

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

@endsection