<form
        method="post"
        action="{{ $group->exists ? route('groups.update', $group->id) : route('groups.store')}}"
        class="form-horizontal"
>
    {{csrf_field()}}

    @if($group->exists)
        {{method_field('put')}}
    @endif

    <div class="titles">
        <h2>Добави детска група</h2>
    </div>
    <div class="form-group {{$errors->has('title') ? 'has-error'  : '' }}">
        <label class="col-sm-3 control-label form-label">Име</label>
        <div class="col-sm-9">
            <input type="text" name="title" class="form-control" value="{{ old('title', $group->title) }}" >
            @if ($errors->has('title'))
                <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
            @endif
        </div>
    </div>

    <div class="form-group {{$errors->has('description') ? 'has-error'  : '' }}">
        <label class="col-sm-3 control-label form-label">Описание</label>
        <div class="col-sm-9">
            <textarea id="description" name="description">{{ old('description', $group->description) }}</textarea>

            @if ($errors->has('description'))
                <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group {{$errors->has('group_age_id') ? 'has-error'  : '' }}">
        <label class="col-sm-3 control-label form-label">Възрастова група</label>
        <div class="col-sm-9">
            <select class="form-control selectpicker col-md-12  display-block" name="group_age_id">
                <option value="">Изберете възрастова група</option>

                <?php  $old = old('group_age_id', $group->groupAge ? $group->groupAge->id : '') ?>

                @foreach($groupAges as $id => $title)
                    <option {{ $old == $id ? 'selected="selected' : ''  }} value="{{$id}}">{{$title}}</option>
                @endforeach
            </select>

            @if ($errors->has('group_age_id'))
                <span class="help-block">
                            <strong>{{ $errors->first('group_age_id') }}</strong>
                        </span>
            @endif
        </div>
    </div>

    <div class="form-group {{$errors->has('teachers') ? 'has-error'  : '' }}">
        <label class="col-sm-3 control-label form-label">Учители</label>
        <div class="col-sm-9">
            <select name="teachers[]" class="form-control selectpicker col-md-12  display-block" multiple="multiple">
                @foreach($teachers as $id => $name)
                    <option {{ in_array($id, old('teachers', []))  ? 'selected="selected"' : '' }} value="{{$id}}">{{$name}}</option>
                @endforeach
            </select>

            @if ($errors->has('teachers'))
                <span class="help-block">
                            <strong>{{ $errors->first('teachers') }}</strong>
                        </span>
            @endif

        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label form-label"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-default pull-right">Запази</button>
        </div>
    </div>
</form>

@section('scripts')
    <script>
        var simplemde = new SimpleMDE({ element: document.getElementById("description") });

    </script>
@endsection

