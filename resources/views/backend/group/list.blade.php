<div class="row">
    <div class="col-lg-12">
        <div class="titles">
            <h2>Списък с детски групи</h2>
        </div>

        <div class="panel panel-default">

            <div class="panel-body table-responsive">

                <div id="example0_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="example0_length">

                    </div>

                    <table id="example0" class="table display dataTable" role="grid" aria-describedby="example0_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 253px;">Име</th>
                            <th class="sorting" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 400px;">Възрастова група</th>
                            <th>Учители</th>
                            <th>Програма</th>
                            <th class="sorting text-right" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 186px;">Деца</th>
                        </tr>
                        </thead>


                        <tbody>
                        @foreach($groups as $key => $group)
                            <tr role="row" class="{{$key % 2 != 0 ? 'odd' : 'even' }}">
                                <td>{{$group->title}}</td>
                                <td>{{$group->groupAge->title}}</td>
                                <td>{{implode(',', $group->teachers->pluck('name')->toArray())}}</td>
                                <td><a href="{{route('group.schedule.create', $group->id)}}" class="btn btn-primary" ><i class="fa fa-calendar"></i></a></td>
                                <td class="text-right"><a href="{{route('groups.show', $group->id)}}" class="btn btn-default">Списък с деца</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>