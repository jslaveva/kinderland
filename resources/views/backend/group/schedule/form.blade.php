@extends('layouts.backend.app')
@section('title', 'Създаване на учителски профил')
@section('content')
    <div class="row presentation">
        <div class="col-lg-8 col-md-6 titles">
            <h1>Програма на {{$group->title}} </h1>
            <h4>попълнете формата за да добавите седмична програма на групата</h4>
        </div>
    </div>

    <div class="container-default">
        <form action="{{route('group.schedule.store', $group->id)}}" method="post">
            {{csrf_field()}}
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Понеделник</th>
                    <th>Вторник</th>
                    <th>Сряда</th>
                    <th>Четвъртък</th>
                    <th>Петък</th>
                </tr>
                </thead>

                <tfoot id="tfooter" >
                <tr>
                    <td> <a data-day="1"> <i  class="fa fa-plus"></i></a></td>
                    <td> <a data-day="2"><i  class="fa fa-plus"></i></a></td>
                    <td> <a data-day="3"><i  class="fa fa-plus"></i></a></td>
                    <td> <a data-day="4"><i  class="fa fa-plus"></i></a></td>
                    <td> <a data-day="5"><i  class="fa fa-plus"></i></a></td>
                </tr>
                </tfoot>
                <tbody>

                <?php $days = [ 1=> 'Понеделник', 2=> 'Вторник', 3=> 'Сряда', 4=> 'Четвъртък', 5=> 'Петък']; ?>

                <tr>
                    @foreach($days as $index => $day)
                        <td data-indexday="{{$index}}" >

                            @if(in_array($day,  array_keys($group->orderSchedule())))
                                @foreach($group->orderSchedule()[$day] as $hours)
                                    <div class="form-group">
                                        <select class="form-control" name="weekday[{{$index}}][]" >
                                            @foreach($subjects as $subject)
                                                <option {{ $hours->subject->id == $subject->id ? 'selected="selected"' : ''  }} value="{{$subject->id}}">{{$subject->name}}</option>
                                            @endforeach
                                        </select>
                                        <i class="pull-right fa fa-minus-circle text-danger" onclick="$(this).parent().remove()" ></i>
                                    </div>
                                @endforeach
                            @endif
                        </td>
                    @endforeach
                </tr>
                </tbody>
            </table>

            <br />
            <button type="submit" class="btn btn-default pull-right">Запази</button>
        </form>

    </div>


@endsection

@section('scripts')
    <script>

        function addInput(day) {
            var html = '<div class="form-group">';

            html += '<select class="form-control" name="weekday['+day+'][]" >';
            @foreach($subjects as $subject)
                html +=  '<option value="{{$subject->id}}" >{{$subject->name}}</option>';
            @endforeach
                html += '</select>';
            html += ' <i class="pull-right fa fa-minus-circle text-danger" onclick="$(this).parent().remove()" ></i> </div>';
            return html;
        }


        $('#tfooter a').on('click', function(e){
            e.preventDefault();
            var day = $(this).data('day');

            $('td[data-indexday="'+day+'"]').append(addInput(day));


        })

    </script>
@endsection