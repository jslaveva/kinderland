@extends('layouts.backend.app')
@section('title', 'Детски групи')
@section('content')
    <div class="presentation">
        <div class="row titles">
            <div class="col-lg-1 col-md-2">
                <span class="icon title-icon color8-bg"><i class="fa fa-child"></i></span>
            </div>
            <div class="col-lg-10">
                <h1>Добавяне на детска група</h1>
                <h4>попълнете формата за да добавите нова детска група в определена възрастова група</h4>
            </div>
        </div>
    </div>

    <div class="container-default">
        <div class="row">
            <div class="col-lg-6">
                @include('backend.group.form')
            </div>
        </div>

        @include('backend.group.list')
    </div>
@endsection