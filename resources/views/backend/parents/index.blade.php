@extends('layouts.backend.app')
@section('title', 'Родители')
@section('content')
    <div class="presentation">
        <div class="row titles">
            <div class="col-lg-1 col-md-2">
                <span class="icon title-icon color8-bg"><i class="fa fa-female"></i></span>
            </div>
            <div class="col-lg-10">
                <h1>Родители</h1>
                <h4>регистрирани в системата родители</h4>
            </div>
        </div>

    </div>

    <div class="container-padding">
        <div class="row">
            <div class="col-md-12">

                <div class="pull-right">
                    <a class="btn btn-default" href="kinder_add_user.html">
                        <i class="fa fa-plus"></i> Добави родител
                    </a>
                </div>


                <div class="clearfix"></div>
                <br>

                <div class="panel panel-default">
                    <div class="panel-body table-responsive">

                        <div id="example0_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="example0_length">

                            </div>


                            <div id="example0_filter" class="dataTables_filter"><label>Търсене:<input type="search" class="" placeholder="" aria-controls="example0"></label></div>

                            <table id="example0" class="table display dataTable" role="grid" aria-describedby="example0_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 253px;">Имена</th>
                                    <th>Емайл</th>
                                    <th>Телефон</th>
                                    <th>Адрес</th>
                                    <th class="sorting" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 400px;">Дете/ца</th>
                                    <th class="sorting text-right" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 207px;">Действия</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach($parents as $parent)
                                    <tr role="row" class="odd">
                                        <td>{{$parent->name}}</td>
                                        <td>{{$parent->email}}</td>
                                        <td>{{$parent->phone}}</td>
                                        <td>{{$parent->address}}</td>
                                        <td>{{implode(', ', $parent->children->pluck('name')->toArray())}}а</td>
                                        <td class="text-right">
                                            <div class="btn-group" role="group" aria-label="...">
                                                <button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach



                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection