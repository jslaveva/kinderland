@extends('layouts.backend.app')
@section('title', 'Добавяне на модул към'. $subject->name)
@section('content')
    <div class="row presentation">
        <div class="col-lg-12 titles">
            <h1>Добавяне на модул към {{$subject->name}} </h1>
            <h4>попълнете предметите, модулите и задачите за учебния план</h4>
        </div>
    </div>

    <div class="container-default">
        <form class="form-horizontal" action="{{route('subjects.modules.store', $subject->id)}}" method="post">
            {{csrf_field()}}


            <div class="panel">



                <div class="form-group required {{$errors->has('title') ? 'has-error'  : '' }}">
                    <label for="input002" class="col-sm-2 control-label form-label">Заглавие на модул</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="title">
                        @if ($errors->has('title'))
                            <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <hr>

                <div id="module-section">
                    <?php $row = 0; ?>
                    @foreach(old('assignment', []) as $k => $assignment)

                        <div class="form-group required {{$errors->has('assignment.'.$k.'.assignment') ? 'has-error'  : '' }}">
                            <label for="input002" class="col-sm-2 control-label form-label">Задача</label>
                            <div class="col-sm-2">
                                <textarea name="assignment[{{$row}}][assignment]" id="" cols="30" rows="3" style="margin: 0px; width: 319px; height: 85px;">{{$assignment['assignment']}}</textarea>

                                @if ($errors->has('assignment.'.$k.'.assignment'))
                                    <span class="help-block">
                            <strong>{{ $errors->first('assignment.'.$k.'.assignment') }}</strong>
                        </span>
                                @endif

                            </div>
                            <label class="col-sm-2 control-label form-label">Група</label>
                            <div class="col-sm-4">
                                <select name="assignment[{{$row}}][group_age_id]"  class="col-md-12 selectpicker selectpicker">
                                    @foreach($groupAges as $id => $title)
                                        <option {{$assignment['group_age_id']  == $id ? 'selected="selected' : '' }}  value="{{$id}}">{{$title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <?php $row++;  ?>
                    @endforeach
                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label form-label"></label>
                    <div class="col-sm-10">
                        <a id="add-new-module" class="btn btn-primary pull-right">Добавяне на задача към модул</a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label form-label"></label>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-default pull-right">Запази</button>
                    </div>
                </div>

            </div>
        </form>
    </div>


@endsection

@section('scripts')

    <script>
        var row = '<?php echo $row; ?>';

        function assignment(row) {
            var html = '';
            html += '<div class="form-group required">';
            html += '<label for="input002" class="col-sm-2 control-label form-label">Задача</label>';
            html += '<div class="col-sm-2">';
            html += '<textarea name="assignment['+row+'][assignment]" id="" cols="30" rows="3" style="margin: 0px; width: 319px; height: 85px;"></textarea>';
            html += '</div>';
            html += '<label class="col-sm-2 control-label form-label">Група</label>'
            html += '<div class="col-sm-4">';
            html += '<select name="assignment['+row+'][group_age_id]"  class="col-md-12 selectpicker selectpicker">';
            @foreach($groupAges as $id => $title)
                html +='<option  value="{{$id}}">{{$title}}</option>';
            @endforeach

                html += '</select>';
            html += '</div>'
            html += '</div>';
            return html
        }


        $('#add-new-module').on('click', function(e) {
            e.preventDefault();
            $('#module-section').append(assignment(row));
            row++;
        });
    </script>
@endsection