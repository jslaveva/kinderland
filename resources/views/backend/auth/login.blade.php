<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <title>Вход</title>

    <link href="{{ asset('css/backend/app.css') }}" rel="stylesheet">
    <style type="text/css">
        body{background: #F5F5F5;}
    </style>
</head>
<body>

<div class="login-form">
    <form action="/backend/login" method="post">
        {{ csrf_field() }}
        <div class="top">
            <img src="/img/logo.jpg" alt="">
            <h1>Web Kindeland</h1>
            <h4>вход като администратор</h4>
        </div>
        <div class="form-area">
            <div class="group {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="text" name="email" class="form-control" placeholder="Email">
                <i class="fa fa-user"></i>
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="group {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password"  name="password" class="form-control" placeholder="Парола">
                <i class="fa fa-key"></i>
                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-default btn-block">Вход</button>
        </div>
    </form>
    <div class="footer-links row">
        <div class="col-xs-6 text-right"><a href="#"><i class="fa fa-lock"></i> Забравена парола</a></div>
    </div>
</div>

</body>
</html>