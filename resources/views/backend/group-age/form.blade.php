<form class="form-horizontal" method="post" action="{{ $groupAge->exists ? route('group-age.update', $groupAge->id) : route('group-age.store')}}" >
    {{csrf_field()}}

    @if($groupAge->exists)
        {{method_field('put')}}
    @endif

    <div class="titles">


        <h2>{{$groupAge->exists ? '' : 'Добави възрастова група' }} </h2>
    </div>
    <div class="form-group {{$errors->has('title') ? 'has-error'  : '' }}">
        <label  class="col-sm-2 control-label form-label">Име</label>
        <div class="col-sm-10">
            <input type="text" name="title" class="form-control" value="{{ old('title', $groupAge->title) }}" >
            @if ($errors->has('title'))
                <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
            @endif
        </div>
    </div>

    <div class="form-group {{$errors->has('from') ? 'has-error'  : '' }} ">
        <label for="input002" class="col-sm-2 control-label form-label">Години от</label>
        <div class="col-sm-10">
            <input type="text" name="from" class="form-control" value="{{ old('from', $groupAge->from) }}" >
            @if ($errors->has('from'))
                <span class="help-block">
                            <strong>{{ $errors->first('from') }}</strong>
                        </span>
            @endif
        </div>
    </div>

    <div class="form-group {{$errors->has('to') ? 'has-error'  : '' }}">
        <label class="col-sm-2 control-label form-label">Години до</label>
        <div class="col-sm-10">
            <input type="text" name="to" value="{{ old('to', $groupAge->to) }}" class="form-control" id="input002">
            @if ($errors->has('to'))
                <span class="help-block">
                            <strong>{{ $errors->first('to') }}</strong>
                        </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label form-label"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-default pull-right">Запази</button>
        </div>
    </div>
</form>