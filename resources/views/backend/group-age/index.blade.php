@extends('layouts.backend.app')
@section('title', 'Възрастови групи')
@section('content')
    <div class="presentation">
        <div class="row titles">
            <div class="col-lg-1 col-md-2">
                <span class="icon title-icon color8-bg"><i class="fa fa-child"></i></span>
            </div>
            <div class="col-lg-10">
                <h1>Добавяне на възрастова група</h1>
                <h4>попълнете формата за да добавите нова възрастова групата</h4>
            </div>
        </div>
    </div>

    <div class="container-default">
        <div class="row">
            <div class="col-lg-4">
                @include('backend.group-age.form')
            </div>

            <div class="col-lg-8">
                <div class="titles">
                    <h2>Списък с възрастови групи</h2>
                </div>

                <div class="panel panel-default">

                    <div class="panel-body table-responsive">

                        <div id="example0_wrapper" class="dataTables_wrapper"><div class="dataTables_length" id="example0_length">

                            </div>

                            <table id="example0" class="table display dataTable" role="grid" aria-describedby="example0_info">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 253px;">Име</th>

                                    <th class="sorting" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 400px;">Години от</th>
                                    <th class="sorting" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 186px;">Години до</th>
                                    <th class="sorting" tabindex="0" aria-controls="example0" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 186px;">Действия</th>
                                </tr>
                                </thead>



                                <tbody>

                                @foreach($groupAges as $k => $groupAge)
                                    <tr role="row" class="{{ $k %2 == 0 ? 'odd' : 'event' }}">
                                        <td>{{$groupAge->title}}</td>
                                        <td>{{$groupAge->from}}</td>
                                        <td>{{$groupAge->to}}</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="...">
                                                <a href="{{route('group-age.edit', $groupAge->id)}}"  class="btn btn-default"><i class="fa fa-pencil"></i></a>
                                                <form action="{{ route('group-age.destroy', $groupAge->id) }}" method="POST" style="display: inline-block; ">
                                                    {{ csrf_field() }}
                                                    {{method_field('delete')}}
                                                    <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection