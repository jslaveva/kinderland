@extends('layouts.backend.app')
@section('title', 'Редактиране на '.$groupAge->title)
@section('content')
    <div class="presentation">
        <div class="row titles">
            <div class="col-lg-1 col-md-2">
                <span class="icon title-icon color8-bg"><i class="fa fa-child"></i></span>
            </div>
            <div class="col-lg-10">
                <h1>Редакция на възрастова група {{$groupAge->title}} </h1>
            </div>
        </div>
    </div>

    <div class="container-default">
        <div class="row">
            <div class="col-lg-12">
                    @include('backend.group-age.form')
            </div>
        </div>
    </div>

@endsection