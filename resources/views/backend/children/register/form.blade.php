@extends('layouts.backend.app')
@section('title', 'Добавяне на дете')
@section('content')
    <div class="presentation">
        <div class="row titles">
            <div class="col-lg-1 col-md-2">
                <span class="icon title-icon color8-bg"><i class="fa fa-plus"></i></span>
            </div>
            <div class="col-lg-10">
                <h1>Добавяне на потребители</h1>
                <h4>попълнете формата за да добавите ново дете и родител към вашето учебно заведение</h4>
            </div>
        </div>
    </div>


    <div class="container-default">
        <div class="row">
            <div class="col-md-12 padding-0">
                <div class="panel panel-transparent">

                    <form class="form-horizontal" action="{{route('register-child.store')}}" method="post" enctype="multipart/form-data" >
                        {{csrf_field()}}
                        <div class="panel-body">
                            <div role="tabpanel">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li role="presentation" class="active"><a href="#child" aria-controls="child" role="tab" data-toggle="tab" aria-expanded="true" class="active">Регистрация на дете</a></li>
                                    <li role="presentation" class=""><a href="#parent" aria-controls="parent" role="tab" data-toggle="tab" class="" aria-expanded="false">Регистрация на родител</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    @include('backend.children.register.child')
                                    @include('backend.children.register.parent')

                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function chooseFile(e) {
            $("#fileInput").click();
        }

        function assignFilePreviews() {
            $( 'input[data-previewable=\"true\"]' ).change(function() {
                var prvCnt = $(this).attr('data-preview-container');
                if(prvCnt) {
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var img = $('<img>', {width: '184px', height:'227px'});
                            img.attr('src', e.target.result);
                            img.error(function() {
                                $(prvCnt).html('');
                            });
                            $(prvCnt).html('');
                            img.appendTo(prvCnt);
                        }
                        reader.readAsDataURL(this.files[0]);
                    }
                }
            });
        }
        $(document).ready(function() {
            assignFilePreviews();


        });

    </script>
@endsection