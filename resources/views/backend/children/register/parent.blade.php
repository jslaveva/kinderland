<div role="tabpanel" class="tab-pane" id="parent">
    <div class="row">
        <div class="col-sm-10 col-sm-push-2">
            <h2 class="margin-b-0">Родителят има регистрация?</h2>
            <h5 class="margin-t-0">ако родителят вече има регистрация в системата, моля изберете името му от списъка</h5>
        </div>
        <div class="form-group col-lg-12">
            <label class="col-sm-2 control-label form-label"></label>
            <div class="col-sm-10">
                <select name="parent_id" class="col-md-12 selectpicker selectpicker required" >
                    @foreach($parents as $parent)
                        <option value="">Изберете родител</option>
                        <option {{old('parent_id')  == $parent->id ? 'selected="selected"' : '' }} value="{{$parent->id}}">{{$parent->name}} ({{$parent->email}}) </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10 col-sm-push-2 margin-b-35">
            <h2 class="margin-b-0">Регистрация на родител</h2>
            <h5 class="margin-t-0">попълнете необходимата информация за да добавите нов родител към системата на учебното заведение</h5>
        </div>
    </div>
    <div class="form-group required {{$errors->has('parent.name') ? 'has-error'  : '' }}">
        <label for="input002" class="col-sm-2 control-label form-label">Имена</label>
        <div class="col-sm-10">
            <input type="text" name="parent[name]" value="{{ old('parent.name') }}" class="form-control">
            @if ($errors->has('parent.name'))
                <span class="help-block">
                    <strong>{{ $errors->first('parent.name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group required {{$errors->has('parent.phone') ? 'has-error'  : '' }} ">
        <label for="input002" class="col-sm-2 control-label form-label">Телефон</label>
        <div class="col-sm-10">
            <input type="text" name="parent[phone]" class="form-control" value="{{ old('parent.phone') }}">
            @if ($errors->has('parent.phone'))
                <span class="help-block">
                    <strong>{{ $errors->first('parent.phone') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <div class="form-group required {{$errors->has('parent.email') ? 'has-error'  : '' }}">
        <label for="input002" class="col-sm-2 control-label form-label">Email</label>
        <div class="col-sm-10">
            <input type="email" name="parent[email]" class="form-control" value="{{ old('parent.email') }}">
            @if ($errors->has('parent.email'))
                <span class="help-block">
                    <strong>{{ $errors->first('parent.email') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group required {{$errors->has('parent.password') ? 'has-error'  : '' }}">
        <label for="input002" class="col-sm-2 control-label form-label">Парола</label>
        <div class="col-sm-10">
            <input type="password" name="parent[password]" class="form-control">
            @if ($errors->has('parent.email'))
                <span class="help-block">
                    <strong>{{ $errors->first('parent.password') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group required {{$errors->has('parent.address') ? 'has-error'  : '' }}">
        <label class="col-sm-2 control-label form-label">Адрес</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="parent[address]" rows="3" id="textarea1">{{ old('parent.address') }}</textarea>
            @if ($errors->has('parent.address'))
                <span class="help-block">
                    <strong>{{ $errors->first('parent.address') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label form-label"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-default pull-right">Запази</button>
        </div>
    </div>
</div>