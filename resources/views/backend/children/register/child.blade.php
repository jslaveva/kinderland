<div role="tabpanel" class="tab-pane padding-b-50 active" id="child">

    <div class="row">
        <div class="col-sm-10 col-sm-push-2 margin-b-35">
            <h2 class="margin-b-0">Регистрация на дете</h2>
            <h5 class="margin-t-0">попълнете необходимата информация за да добавите ново дете към системата на учебното заведение</h5>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2"></label>
        <div class="col-sm-10 profile-center">

            <div class = "prd-img-prv">
                <img src="/img/image_preview.png" alt="img" class="profile-img">
            </div>

        </div>
    </div>

    <div class="form-group {{$errors->has('picture') ? 'has-error'  : '' }}">
        <label class="col-sm-2"></label>
        <div class="col-sm-10 profile-center">
            <button onclick="chooseFile()"; type="button" class="btn btn-default"><i class="fa fa-picture-o"></i>Качи снимка</button>
            <input type="file"  name="picture"  class="hidden" id="fileInput"   data-previewable="true" data-preview-container=".prd-img-prv" />

            @if ($errors->has('picture'))
                <span class="help-block">
                            <strong>{{ $errors->first('picture') }}</strong>
                        </span>
            @endif


        </div>
    </div>

    <div class="form-group required {{$errors->has('child.name') ? 'has-error'  : '' }} ">
        <label for="input002" class="col-sm-2 control-label form-label">Имена</label>
        <div class="col-sm-10">
            <input type="text"  name="child[name]" value="{{ old('child.name') }}" class="form-control" id="input002">
            @if ($errors->has('child.name'))
                <span class="help-block">
                    <strong>{{ $errors->first('child.name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group required {{$errors->has('child.egn') ? 'has-error'  : '' }}">
        <label for="input002" class="col-sm-2 control-label form-label">ЕГН</label>
        <div class="col-sm-10">
            <input type="text" name="child[egn]" value="{{ old('child.egn') }}" class="form-control">
            @if ($errors->has('child.egn'))
                <span class="help-block">
                    <strong>{{ $errors->first('child.egn') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group required {{$errors->has('child.group_id') ? 'has-error'  : '' }} ">
        <label class="col-sm-2 control-label form-label">Група</label>
        <div class="col-sm-10">
            <select name="child[group_id]" class="col-md-12 selectpicker selectpicker required" >
                <option value="">Изберете група</option>
               @foreach($groups as $id => $title)
                    <option {{old('child.group_id')  == $id ? 'selected="selected"' : '' }} value="{{$id}}"> {{$title}}</option>
               @endforeach
            </select>
            @if ($errors->has('child.group_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('child.egn') }}</strong>
                </span>
            @endif
        </div>
    </div>


</div>