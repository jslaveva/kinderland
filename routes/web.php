<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');
Route::get('/news', 'NewsController@index')->name('news');
Route::get('/team', 'TeamController@index')->name('team');
Route::get('/schedule', 'ScheduleController@index')->name('schedule');
Route::get('/groups', 'GroupsController@index')->name('groups');



//Route::namespace('Backend')->prefix('backend')->group(function () {
Route::group(['namespace' => 'Backend', 'prefix' => 'backend'], function () {

    Route::get('/login', 'LoginController@showLoginForm');
    Route::post('/login', 'LoginController@login');

    Route::group(['middleware' => 'auth' ], function() {


        Route::resource('group-age', 'GroupAgeController');
        Route::resource('groups', 'GroupsController');

        Route::get('/schedule/{id}', ['as' => 'group.schedule.create', 'uses' => 'ScheduleController@create']);
        Route::post('/schedule/{id}', ['as' => 'group.schedule.store', 'uses' => 'ScheduleController@store']);

        Route::resource('teachers', 'TeachersController');
        Route::resource('subjects', 'SubjectsController');
        Route::resource('subjects.modules', 'ModulesController');

        Route::get('register-child', ['as' => 'register-child.create', 'uses' => 'RegisterChildController@create']);
        Route::post('register-child', ['as' => 'register-child.store', 'uses' => 'RegisterChildController@store']);
        Route::resource('parents', 'ParentsController');
    });

});