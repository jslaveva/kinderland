<?php

use App\Services\FlashMessenger;

/**
 * @param null $title
 * @param null $message
 * @return FlashMessenger
 */
function flash($title = null, $message = null)
{
    $flash = app(FlashMessenger::class);
    if (func_num_args() == 0) {
        return $flash;
    }
    return $flash->info($title, $message);
}

if (!function_exists('menuActiveRoute')) {
    function menuActiveRoute($routeName, $includeClass = false, $className = 'active')
    {
        $activeClass = ($includeClass ? "class=\"{$className}\"": $className);
        return preg_match('/^' . preg_quote($routeName, '/') . '/', Request::route()->getName()) ? $activeClass: '';
    }
}