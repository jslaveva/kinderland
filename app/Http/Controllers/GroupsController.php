<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;

class GroupsController extends Controller
{
    public function index()
    {
        $groups = Group::with('teachers')->get();
        return view('frontend.groups.index', ['groups' => $groups]);
    }
}
