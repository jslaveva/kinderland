<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Teacher;
use App\Models\TeacherType;
use Illuminate\Http\Request;

class TeachersController extends Controller
{
    public function index()
    {
        $teachers = Teacher::with('type')->paginate(5);
        return view('backend.teachers.index', ['teachers' => $teachers]);
    }

    public function create()
    {
        return view('backend.teachers.create', ['teacherTypes' => TeacherType::pluck('title', 'id')]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:teachers,email',
            'phone' => 'required',
            'bio' => 'required',
            'picture' => 'required|image',
            'teacher_type_id' => 'required|exists:teacher_types,id'
        ]);


        $fields = $request->only(['name', 'bio', 'email', 'phone', 'teacher_type_id']);

        $fields['picture'] = $path = $request->picture->store('images', 'public');

        $teacher = Teacher::create($fields);
        flash()->success('Успех', 'Учител '.$teacher->name.' е добавен');

        return redirect()->route('teachers.index');

    }

    public function edit($id)
    {
        $teacher = Teacher::findOrFail($id);
        return view('backend.teachers.edit', ['teacher' => $teacher, 'teacherTypes' => TeacherType::pluck('title', 'id')]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email'  =>  'required|email|unique:teachers,email,'.$id,
            'phone' => 'required',
            'bio' => 'required',
            'teacher_type_id' => 'required|exists:teacher_types,id'
        ]);

        $teacher = Teacher::findOrFail($id);
        $teacher->update($request->only(['name', 'bio', 'email', 'phone', 'teacher_type_id']));

        flash()->success('Успех', 'Учител '.$teacher->name.' е обновен');
        return redirect()->route('teachers.index');

    }

    public function destroy($id)
    {
        $teacher = Teacher::findOrFail($id);
        $teacher->delete();
        flash()->success('Успех', 'Учител '.$teacher->name.' е изтрит');
        return redirect()->back();
    }
}
