<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Child;
use App\Models\ChildParent;
use App\Models\Group;
use Illuminate\Http\Request;

class RegisterChildController extends Controller
{
    public function create()
    {
        $parents = ChildParent::all();
        $groups = Group::pluck('title', 'id');
        return view('backend.children.register.form', [
            'parents' => $parents,
            'groups' => $groups
        ]);
    }

    public function store(Request $request)
    {

        $this->validate($request, $this->rules($request));
        $child = !is_null($request->parent_id) ? Child::registerWithExistingParent($request) : Child::registerWithNewParent($request);
        flash()->success('Успех', $child->name.' е добавен/а в системата');
        return redirect()->back();

    }

    protected function rules($request)
    {

        $baseRules = [
            'child.name' => 'required',
            'child.egn' => 'required|integer',
            'child.group_id' => 'required',
            'picture' => 'required|image',
        ];

        if (!$request->parent_id) {

            $parentRules = [
                'parent.name' => 'required',
                'parent.email' => 'required|email|unique:parents,email',
                'parent.phone' => 'required',
                'parent.password' => 'required',
                'parent.address' => 'required',
            ];

            $baseRules += $parentRules;
        }

        return $baseRules;
    }
}
