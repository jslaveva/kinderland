<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ChildParent;
use Illuminate\Http\Request;

class ParentsController extends Controller
{
    public function index()
    {
        $parents = ChildParent::with('children')->paginate(5);
        return view('backend.parents.index', ['parents' => $parents]);
    }
}
