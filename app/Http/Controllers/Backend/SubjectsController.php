<?php
/**
 * Created by PhpStorm.
 * User: sylar
 * Date: 25.06.17
 * Time: 14:19
 */

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Assignment;
use App\Models\GroupAge;
use App\Models\Module;
use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{
    public function index()
    {
        $subjects = Subject::withCount('modules')->get();
        return view('backend.schedule.index', ['subjects' => $subjects]);
    }

    public function create()
    {
        return view('backend.schedule.create', [
            'groupAges' => GroupAge::pluck('title', 'id'),
        ]);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'title' => 'required',
            'assignment.*.assignment' => 'required',
        ]);

        $subject = Subject::create($request->only('name'));

        /** @var Module $module */
        $module = Module::create(['subject_id' => $subject->id, 'title' => $request->get('title')]);

        foreach ($request->assignment as $assignment) {
            Assignment::create(['module_id' => $module->id, 'assignment' => $assignment['assignment'], 'group_age_id' => $assignment['group_age_id']]);
        }


        flash()->success('Успех', 'Предмет '.$subject->name.' е добавен в системата');
        return redirect()->back();

    }
}
