<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\GroupAge;
use Illuminate\Http\Request;

class GroupAgeController extends Controller
{
    public function index()
    {
        $groupAges = GroupAge::all();
        return view('backend.group-age.index', ['groupAge' => new GroupAge, 'groupAges' => $groupAges ]);

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'from'  => 'required|integer',
            'to'  => 'required|integer',
        ]);

        $groupAge = GroupAge::create($request->only(['title', 'from', 'to']));
        flash()->success('Успех', 'Възрастова група '.$groupAge->title.' е създадена');
        return redirect()->back();
    }

    public function edit($id)
    {
        $groupAge = GroupAge::findOrFail($id);
        return view('backend.group-age.edit', ['groupAge' => $groupAge]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'from'  => 'required|integer',
            'to'  => 'required|integer',
        ]);

        /** @var GroupAge $groupAge */
        $groupAge = GroupAge::findOrFail($id);
        $groupAge->update($request->only(['title', 'from', 'to']));

        flash()->success('Успех', 'Възрастова група '.$groupAge->title.' е редактирана');
        return redirect()->route('group-age.index');

    }

    public function destroy($id)
    {
        /** @var GroupAge $groupAge */
        $groupAge = GroupAge::withCount('groups')->findOrFail($id);

        if ($groupAge->groups_count != 0) {
            flash()->error('Възникна грешка', 'Възрастовата група '.$groupAge->title.' не може да бъде изтрита');
        } else {
            $groupAge->delete();
            flash()->success('Успех', 'Възрастова група '.$groupAge->title.' е изтрита');
        }

        return redirect()->route('group-age.index');
    }
}
