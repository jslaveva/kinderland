<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Assignment;
use App\Models\GroupAge;
use App\Models\Module;
use App\Models\Subject;
use Illuminate\Http\Request;

class ModulesController extends Controller
{
    public function create($subjectId)
    {
        $subject = Subject::findOrFail($subjectId);
        return view('backend.modules.create', [
            'groupAges' => GroupAge::pluck('title', 'id'),
            'subject' => $subject
        ]);
    }

    public function store($subjectId, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'assignment.*.assignment' => 'required',
        ]);

        /** @var Module $module */
        $module = Module::create(['subject_id' => $subjectId, 'title' => $request->get('title')]);

        foreach ($request->assignment as $assignment) {
            Assignment::create(['module_id' => $module->id, 'assignment' => $assignment['assignment'], 'group_age_id' => $assignment['group_age_id']]);
        }

        flash()->success('Успех', 'Модул '.$module->title.' е добавен в системата');
        return redirect()->back();

    }
}
