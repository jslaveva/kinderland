<?php
/**
 * Created by PhpStorm.
 * User: sylar
 * Date: 25.06.17
 * Time: 23:27
 */

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Schedule;
use App\Models\Subject;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function create($groupId)
    {
        $group = Group::findOrFail($groupId);
        $subjects = Subject::all();
        return view('backend.group.schedule.form', [
            'group' => $group,
            'subjects' => $subjects
        ]);
    }

    public function store($id, Request $request)
    {
        $group = Group::findOrFail($id);

        Schedule::where('group_id', $id)->delete();

        foreach($request->get('weekday') as $k => $subjects ) {

            foreach ($subjects as $subject) {
                Schedule::create([
                    'weekday' => $k,
                    'subject_id' => $subject,
                    'group_id' => $id
                ]);
            }

        }

        flash()->success('Успех', 'Програмата на'. $group->title.' е обновена');
        return redirect()->back();

    }

}