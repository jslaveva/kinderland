<?php

namespace App\Http\Controllers;

use App\Models\GroupAge;
use App\Models\Subject;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function index()
    {
        $subjects = Subject::all();
        $groupAges = GroupAge::all();
        return view('frontend.schedule.index', [
            'subjects' => $subjects,
            'groupAges' => $groupAges,
        ]);
    }
}
