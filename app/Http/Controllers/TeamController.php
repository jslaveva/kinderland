<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index()
    {
        return view('frontend.team.index', ['teachers' => Teacher::with('type')->get()]);
    }
}
