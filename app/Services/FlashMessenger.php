<?php

namespace App\Services;

class FlashMessenger
{
    const LEVEL_INFO = 'info';
    const LEVEL_SUCCESS = 'success';
    const LEVEL_ERROR = 'error';

    private $timer = 2000;

    public function create($title, $message, $notice)
    {
        session()->flash(
            'flash_message',
            ['title' => $title, 'message' => $message, 'notice' => $notice, 'timer' => $this->timer]
        );
    }

    public function info($title, $message)
    {
        $this->create($title, $message, static::LEVEL_INFO);
        return $this;
    }

    /**
     * @param $title
     * @param $message
     * @return $this
     */
    public function success($title, $message)
    {
        $this->create($title, $message, static::LEVEL_SUCCESS);
        return $this;
    }

    public function error($title, $message)
    {
        $this->create($title, $message, static::LEVEL_ERROR);
        return $this;
    }

    public function overlay()
    {
        $this->removeTimer();
        session()->put('flash_message.overlay', 1);
        return $this;
    }

    public function removeTimer()
    {
        $this->timer = null;
        session()->put('flash_message.timer', $this->timer);
    }
}
