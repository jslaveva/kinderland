<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table ='schedule';
    protected $guarded = [];

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }


}
