<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $guarded = [];

    public function groupAge()
    {
        return $this->belongsTo(GroupAge::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teachers()
    {
        return $this->belongsToMany(Teacher::class, 'group_teacher');
    }

    public function schedule()
    {
        return $this->hasMany(Schedule::class);
    }


    public function orderSchedule()
    {
        $result = [];
        foreach ($this->schedule as $record) {
            $result[$record->weekday][] = $record;
        }

        return $result;
    }





}
