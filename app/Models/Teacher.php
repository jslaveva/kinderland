<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $guarded = [];

    public function type()
    {
        return $this->belongsTo(TeacherType::class, 'teacher_type_id');
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
