<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChildParent extends Model
{
    protected $guarded = [];
    protected $table = 'parents';

    public function children()
    {
        return $this->hasMany(Child::class, 'parent_id');
    }
}
