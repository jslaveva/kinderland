<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    protected $guarded = [];

    public static function registerWithExistingParent($request)
    {
        $data = $request->get('child');
        $data['parent_id'] = $request->get('parent_id');
        $data['picture'] = static::uploadPath($request);

        return static::create($data);
    }

    protected static function uploadPath($request)
    {
        return $request->picture->store('images', 'public');
    }

    public static function registerWithNewParent($request)
    {
        $data = $request->get('child');
        $data['picture'] = static::uploadPath($request);
        $parent = new ChildParent($request->get('parent'));
        $parent->save();

        /** @var Child $child */
        $data['parent_id'] = $parent->id;
        return static::create($data);
    }

    public function parent()
    {
        return $this->belongsTo(ChildParent::class, 'parent_id');
    }
}
